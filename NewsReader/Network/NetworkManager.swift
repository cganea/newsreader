//
//  NetworkManager.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import Alamofire
import SwiftyJSON
import ObjectMapper

let BaseURL = "https://hacker-news.firebaseio.com/v0/"

enum NetworkCalls: String {
    case TopStories = "/topstories.json"
    case NewStories = "/newstories.json"
    case Story = "/item/"
}

class NetworkManager {
    
    static let sharedInstance = NetworkManager()
    
    func getTopStories(completion:([Int]) -> Void) {
        Alamofire.request(.GET, BaseURL+NetworkCalls.TopStories.rawValue)
            .responseJSON { response in
            if let JSON = response.result.value {
                completion(JSON as! [Int])
            }
        }
    }
    
    func getNewStories(completion:([Int]) -> Void) {
        Alamofire.request(.GET, BaseURL+NetworkCalls.NewStories.rawValue)
            .responseJSON { response in
            if let JSON = response.result.value {
                completion(JSON as! [Int])
            }
        }
    }
    
    func getStory(id: Int, completion:Story -> Void) {
        Alamofire.request(.GET, BaseURL+NetworkCalls.Story.rawValue+String(id)+".json")
            .responseJSON { response in
            if let JSON = response.result.value {
                if let item = Mapper<Story>().map(JSON) {
                    completion(item)
                }
            }
        }
    }
}

//
//  Story.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import Foundation
import SwiftyJSON
import ObjectMapper

class Story: Mappable {
    
    var by: String?
    var descendants: Int?
    var id: Int?
    var score: Int?
    var time: Int?
    var title: String?
    var type: String?
    var url: String?
    
    required init?(_ map: ObjectMapper.Map) {
        by                  <- map["by"]
        descendants         <- map["descendants"]
        id                  <- map["id"]
        score               <- map["score"]
        time                <- map["time"]
        title               <- map["title"]
        type                <- map["type"]
        url                 <- map["url"]
    }
    
    init(idI: Int, timeI: Int, titleS: String, urlS: String) {
        id = idI
        time = timeI
        title = titleS
        url = urlS
    }
    
    func mapping(map: Map) {
        by                  <- map["by"]
        descendants         <- map["descendants"]
        id                  <- map["id"]
        score               <- map["score"]
        time                <- map["time"]
        title               <- map["title"]
        type                <- map["type"]
        url                 <- map["url"]
    }
}

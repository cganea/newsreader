//
//  StoryDetails.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import UIKit
import WebKit
import SQLite

class StoryDetails: UIViewController, WKUIDelegate {
    
    @IBOutlet weak var address: UILabel!
    
    var storyView: WKWebView!
    var story: Story?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = story?.title
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "download"), style: .Plain, target: self, action: #selector(StoryDetails.saveStory(_:)))
        self.automaticallyAdjustsScrollViewInsets = false
        
        self.address.lineBreakMode = .ByWordWrapping
        self.address.numberOfLines = 0
        self.address.text = story?.url
        
        let addressHeight = self.address.frame.size.height
        
        let webConfiguration = WKWebViewConfiguration()
        storyView = WKWebView(frame: CGRectMake(0, addressHeight+90, view.frame.size.width, view.frame.size.height-(addressHeight+90)), configuration: webConfiguration)
        storyView.UIDelegate = self
        view.addSubview(storyView)

        if let sURL = story?.url, let thisURL = NSURL(string: sURL) {
            storyView.loadRequest(NSURLRequest(URL: thisURL))
        }
    }
    
    func saveStory(sender: UIBarButtonItem) {
        
        let sharedApp = UIApplication.sharedApplication().delegate as! AppDelegate
        let savedStories = Table("savedStories")
    
        if let thisDB = sharedApp.db {
 
            let id = Expression<Int>("id")
            let time = Expression<Int>("time")
            let title = Expression<String>("title")
            let url = Expression<String>("url")
            
            do {
                try thisDB.run(savedStories.create(ifNotExists: true) { t in
                    t.column(id, primaryKey: true)
                    t.column(time)
                    t.column(title)
                    t.column(url)
                })
            } catch {
                print("Error creating table")
            }
            
            do {
                if let sID = story?.id, sTime = story?.time, sTitle = story?.title, sURL = story?.url {
                    try thisDB.run(savedStories.insert(or: .Replace, id <- sID, time <- sTime, title <- sTitle, url <- sURL))
                    print("Story saved to DB")
                    
                    let alert = UIAlertController(title: "Success", message: "Story succesfully saved", preferredStyle: UIAlertControllerStyle.Alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
            } catch {
                let alert = UIAlertController(title: "Error", message: "Story could not be saved", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
}

//
//  StoryCell.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import UIKit

class StoryCell: UITableViewCell {
    
    @IBOutlet weak var storyTitle: UILabel!
    @IBOutlet weak var storyTime: UILabel!
    @IBOutlet weak var markButton: UIButton!
    @IBOutlet weak var icon: UIImageView!
    
    var markAsRead:(Void -> Void)?
    var markForDelete:(Void -> Void)?
    var dateFormatter: NSDateFormatter!

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .None
        
        dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd YYYY hh:mm a"
        self.storyTime.font = UIFont.systemFontOfSize(12)
        self.storyTime.textColor = UIColor.grayColor()
    }
    
    func setup(sTime: Int, sTitle: String, status: Int) {
        icon.image = UIImage(named: "icon")
        storyTime.text = dateFormatter.stringFromDate(NSDate(timeIntervalSince1970: Double(sTime)))
        storyTitle.text = sTitle
        
        let imageArray = [UIImage(named: "flag-empty"), UIImage(named: "flag-filled"), UIImage(named: "favorite"), UIImage(named: "delete")]
        
        markButton.setImage(imageArray[status], forState: .Normal)
        
        if status == 0 || status == 1 {
            markButton.addTarget(self, action: #selector(StoryCell.markRead(_:)), forControlEvents: .TouchUpInside)
        }
        else if status == 2 {
            markButton.removeTarget(self, action: #selector(StoryCell.markRead(_:)), forControlEvents: .TouchUpInside)
        }
        else if status == 3 {
            markButton.addTarget(self, action: #selector(StoryCell.markDelete(_:)), forControlEvents: .TouchUpInside)
        }
    }
    
    func markRead(sender: UIButton) {
        markAsRead?()
    }
    
    func markDelete(sender: UIButton) {
        markForDelete?()
    }
}

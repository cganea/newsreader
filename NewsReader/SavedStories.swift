//
//  SavedStories.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import UIKit
import SQLite

class SavedStories: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var stories: [Story] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Saved for later"
        self.navigationItem.title = "News Reader"
        self.automaticallyAdjustsScrollViewInsets = false
        
        tableView.registerNib(UINib(nibName: "StoryCell", bundle: nil), forCellReuseIdentifier: "StoryCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        stories.removeAll()
        fetchSavedStories()
    }
    
    func fetchSavedStories() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.populateStories()
            dispatch_async(dispatch_get_main_queue()) {
                self.tableView.reloadData()
            }
        }
    }
    
    func populateStories() {
        let sharedApp = UIApplication.sharedApplication().delegate as! AppDelegate
        let savedStories = Table("savedStories")
        
        if let thisDB = sharedApp.db {
            
            let id = Expression<Int>("id")
            let time = Expression<Int>("time")
            let title = Expression<String>("title")
            let url = Expression<String>("url")
        
            do {
                let dbStories = try thisDB.prepare(savedStories)
                for story in dbStories {
                    let createdStory = Story(idI: story[id], timeI: story[time], titleS: story[title], urlS: story[url])
                    stories.append(createdStory)
                }
            } catch {
                print("Error while retrieving saved stories")
            }
        }
    }
}

extension SavedStories: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return stories.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StoryCell") as! StoryCell
        let story = stories[indexPath.row]
        cell.setup(story.time ?? 0, sTitle: story.title ?? "No title available", status: 3)
        cell.markForDelete = { [weak self] in
            
            let sharedApp = UIApplication.sharedApplication().delegate as! AppDelegate
            let savedStories = Table("savedStories")
            
            if let thisDB = sharedApp.db, let wSelf = self {
                
                let id = Expression<Int>("id")
            
                do {
                    let item = savedStories.filter(id == story.id ?? 0)
                    try thisDB.run(item.delete())
                    if let sID = story.id, let index = wSelf.stories.indexOf({ $0.id == sID }) {
                        wSelf.stories.removeAtIndex(index)
                        wSelf.tableView.reloadData()
                    }
                } catch { print("No result found") }
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let story = stories[indexPath.row]
        let storyView = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("kNRStoryDetails") as! StoryDetails
        storyView.story = story
        self.navigationController?.pushViewController(storyView, animated: true)
    }
}

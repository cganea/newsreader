//
//  ViewController.swift
//  NewsReader
//
//  Created by Cristian Ganea on 27/12/2016.
//  Copyright © 2016 Cristian. All rights reserved.
//

import UIKit
import SQLite

class NewTopStories: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var newStories: [Story] = []
    var topStories: [Story] = []
    var story: Story!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "New & Top Stories"
        self.navigationItem.title = "News Reader"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .Refresh, target: self, action: #selector(NewTopStories.refreshData(_:)))
        self.automaticallyAdjustsScrollViewInsets = false
        
        fetchTopStories()
        
        tableView.registerNib(UINib(nibName: "StoryCell", bundle: nil), forCellReuseIdentifier: "StoryCell")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        tableView.reloadData()
    }
    
    func fetchTopStories() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            self.navigationItem.rightBarButtonItem?.enabled = false
            NetworkManager.sharedInstance.getTopStories() { stories in
                for story in stories {
                    NetworkManager.sharedInstance.getStory(story) { result in
                        self.topStories.append(result)
                        
                        if story == stories.last {
                            self.fetchNewStories()
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func fetchNewStories() {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            NetworkManager.sharedInstance.getNewStories() { stories in
                for story in stories {
                    NetworkManager.sharedInstance.getStory(story) { result in
                        self.newStories.append(result)
                        
                        if story == stories.last {
                            self.navigationItem.rightBarButtonItem?.enabled = true
                        }
                        
                        dispatch_async(dispatch_get_main_queue()) {
                            self.tableView.reloadData()
                        }
                    }
                }
            }
        }
    }
    
    func refreshData(sender: UIBarButtonItem) {
        topStories.removeAll()
        newStories.removeAll()
        fetchTopStories()
    }
    
    func getStatus(sid: Int) -> Int {
        let sharedApp = UIApplication.sharedApplication().delegate as! AppDelegate
        let savedStories = Table("savedStories")
        let readStories = Table("readStories")
        let id = Expression<Int>("id")
        
        var sCount = 0
        var rCount = 0
        
        if let thisDB = sharedApp.db {
            do {
                sCount = try thisDB.scalar(savedStories.filter(id == sid).count)
                rCount = try thisDB.scalar(readStories.filter(id == sid).count)
            } catch { print("No result found") }
        }
        
        if sCount > 0 {
            return 2
        }
        else if rCount > 0 {
            return 1
        }
        else {
            return 0
        }
    }
    
    func markStoryRead(sid: Int) {
        
        let sharedApp = UIApplication.sharedApplication().delegate as! AppDelegate
        let readStories = Table("readStories")
        
        if let thisDB = sharedApp.db {
            
            let id = Expression<Int>("id")
            
            //create table if does not exist
            do {
                try thisDB.run(readStories.create(ifNotExists: true) { t in
                    t.column(id, primaryKey: true)
                    })
            } catch {
                print("Error creating table")
            }
            
            //mark as read if not marked
            do {
                if let sID = story?.id {
                    
                    let item = readStories.filter(id == sID)
                    
                    if try thisDB.scalar(item.count) != 1 {
                        try thisDB.run(readStories.insert(or: .Replace, id <- sID))
                        print("Story saved to DB")
                    }
                    else if try thisDB.scalar(item.count) == 1 {
                        try thisDB.run(item.delete())
                    }
                }
            } catch {
                let alert = UIAlertController(title: "Error", message: "Story could not be marked as read", preferredStyle: UIAlertControllerStyle.Alert)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
                self.presentViewController(alert, animated: true, completion: nil)
            }
        }
    }
    
}

extension NewTopStories: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 {
            return topStories.count
        }
        else {
            return newStories.count
        }
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0 {
            return "Top Stories"
        }
        else {
            return "New Stories"
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("StoryCell") as! StoryCell
        
        if indexPath.section == 0 {
            story = topStories[indexPath.row]
        }
        else {
            story = newStories[indexPath.row]
        }
        
        cell.setup(story.time ?? 0, sTitle: story.title ?? "No title available", status: getStatus(story.id ?? 0))
        cell.markAsRead = { [weak self] in
            
            if let wSelf = self {
                
                if indexPath.section == 0 {
                    wSelf.story = wSelf.topStories[indexPath.row]
                }
                else {
                    wSelf.story = wSelf.newStories[indexPath.row]
                }
                
                if let id = wSelf.story.id {
                    wSelf.markStoryRead(id)
                    wSelf.tableView.reloadData()
                }
            }
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        if indexPath.section == 0 {
            story = topStories[indexPath.row]
        }
        else {
            story = newStories[indexPath.row]
        }
        
        let storyView = UIStoryboard(name: "Main", bundle: nil).instantiateViewControllerWithIdentifier("kNRStoryDetails") as! StoryDetails
        storyView.story = story
        self.navigationController?.pushViewController(storyView, animated: true)
    }
}
